import React, { useEffect, useState } from "react";
import { Reset } from "styled-reset";
import { useFetch } from "./hooks/useFetch";
import { useDebounce } from "./hooks/useDebounce";
import GameService from "./api/GameService";
import GameList from "./components/GameList";
import Search from "./components/Search";
import styled, { createGlobalStyle } from "styled-components";
import Dropdown from "./components/Dropdown/DropDown";

const GlobalStyle = createGlobalStyle`
  body {
    font-size: 14px;
    line-height: normal;
    font-weight: 400;
    font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen,Ubuntu,Cantarell,Open Sans,Helvetica Neue,sans-serif;
    letter-spacing: 0;
    background-color: #151515;
    color: #fff;
  }
`;
const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 60px;
  padding: 6px 10px;

  @media (min-width: 980px) {
    height: 100px;
    padding: 12px 20px;
  }
`;

const sort = [
  {
    label: "rating",
  },
  {
    label: "released",
  },
];

function App() {
  const [games, setGames] = useState([]);
  const [search, setSearch] = useState([]);
  const debouncedSearch = useDebounce(search, 200);

  const [fetchGames] = useFetch(async (search) => {
    const { results } = await GameService.getList(search);
    setGames(results);
  });

  useEffect(() => {
    fetchGames(debouncedSearch);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedSearch]);

  return (
    <React.Fragment>
      <Reset />
      <GlobalStyle />
      <Header>
        <Search value={search} onChange={setSearch} />
      </Header>

      <main>
        <div>
          <Dropdown items={sort} onSelect={(item) => console.log(item)} />
        </div>
        <GameList games={games} />
      </main>
    </React.Fragment>
  );
}

export default App;
