import * as React from "react";

export const DropdownContext = React.createContext();

export function useDropdownContext() {
  const context = React.useContext(DropdownContext);
  if (!context) {
    throw new Error(
      `Dropdown compound components cannot be rendered outside the Dropdown component`
    );
  }
  return context;
}
