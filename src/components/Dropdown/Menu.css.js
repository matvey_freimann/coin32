import styled from "styled-components";

export const MenuAnimationUI = styled("div")`
  transition: all 100ms linear;

  width: ${(props) => props.width}px;
  pointer-events: none;
  opacity: 0;
  transform: translateY(-10px);

  &.in {
    pointer-events: initial;
    opacity: 1;
    transform: translateY(0px);
  }
`;

export const MenuUI = styled("div")`
  background-clip: padding-box;
  background-color: #fff;
  border-radius: 0.25rem;
  border: 1px solid rgba(0, 0, 0, 0.15);
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1), 0 10px 20px rgba(0, 0, 0, 0.05);
  color: #212529;
  font-size: 1rem;
  list-style: none;
  margin: 0.125rem 0 0;
  // min-width: 10rem;
  padding: 0.5rem 0;
  text-align: left;
`;

export const ItemUI = styled("a")`
  background-color: transparent;
  border: 0;
  clear: both;
  color: #212529;
  display: block;
  font-weight: 400;
  padding: 0.25rem 1.5rem;
  text-align: inherit;
  white-space: nowrap;
  width: 100%;
  text-decoration: none;

  &:hover,
  &:focus {
    background-color: #f8f9fa;
    color: #16181b;
    text-decoration: none;
  }

  &:active {
    color: #fff;
    background-color: #007bff;
    text-decoration: none;
  }
`;
