import * as React from "react";
import { DropdownContext, useDropdownContext } from "./Context";

import { Menu } from "./Menu";
import { Portal } from "./Portal";

function Trigger(props) {
  const { toggleOpen, refTrigger } = useDropdownContext();
  const { children } = props;

  return (
    <button ref={refTrigger} onClick={toggleOpen}>
      {children}
    </button>
  );
}

function Container(props) {
  const { items, onSelect } = props;
  const [isOpen, setIsOpen] = React.useState(props.isOpen);
  const refTrigger = React.useRef();
  const refMenu = React.useRef();

  const toggleOpen = React.useCallback(
    () => setIsOpen((nextIsOpen) => !nextIsOpen),
    []
  );

  const contextValue = React.useMemo(
    () => ({
      isOpen,
      items,
      onSelect,
      toggleOpen,
      refTrigger,
      refMenu,
    }),
    [isOpen, items, onSelect, toggleOpen]
  );

  return (
    <DropdownContext.Provider value={contextValue}>
      <div ref={refMenu}>{props.children}</div>
    </DropdownContext.Provider>
  );
}

function Dropdown(props) {
  return (
    <Dropdown.Container {...props}>
      <Dropdown.Trigger>Hello</Dropdown.Trigger>
      <Dropdown.Portal>
        <Dropdown.Menu />
      </Dropdown.Portal>
    </Dropdown.Container>
  );
}

Dropdown.Context = DropdownContext;
Dropdown.Container = Container;
Dropdown.Trigger = Trigger;
Dropdown.Portal = Portal;
Dropdown.Menu = Menu;

export default Dropdown;
