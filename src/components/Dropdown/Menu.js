import * as React from "react";
import { useDropdownContext } from "./Context";
import { usePortalContext } from "./Portal";
import { MenuAnimationUI, MenuUI, ItemUI } from "./Menu.css";

export function MenuAnimation(props) {
  const { children } = props;
  const [width, setWidth] = React.useState(0);
  const { show } = usePortalContext();
  const { refTrigger } = useDropdownContext();

  const className = show ? "in" : "";
  React.useEffect(() => {
    const offset = refTrigger.current.getBoundingClientRect();
    setWidth(offset.width);
  }, [refTrigger]);

  return (
    <MenuAnimationUI width={width} className={className}>
      {children}
    </MenuAnimationUI>
  );
}

export function Menu(props) {
  const { items, onSelect, toggleOpen } = useDropdownContext();

  const createOnClick = (item) => () => {
    onSelect(item);
    toggleOpen();
  };

  return (
    <MenuAnimation>
      <MenuUI>
        {items.map((item) => (
          <Menu.Item {...item} key={item.label} onClick={createOnClick(item)} />
        ))}
      </MenuUI>
    </MenuAnimation>
  );
}

export function Item(props) {
  const { children, href, onClick, label, to } = props;

  const actionLabel = children || label;
  const linkAction = href || to || "#";

  const handleOnClick = React.useCallback(
    (event) => {
      if (linkAction === "#") {
        event.preventDefault();
      }
      onClick && onClick(event);
    },
    [linkAction, onClick]
  );

  return (
    <ItemUI href={linkAction} onClick={handleOnClick}>
      {actionLabel}
    </ItemUI>
  );
}

Menu.Item = Item;

export default Menu;
