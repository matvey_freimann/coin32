import * as React from "react";
import { useDropdownContext } from "./Context";
import { PortalUI } from "./Portal.css";
import ReactDOM from "react-dom";
import { useOnClickOutside } from "../../hooks/useOnClickOutside";

export const PortalContext = React.createContext();

export function usePortalContext() {
  const context = React.useContext(PortalContext);
  if (!context) {
    throw new Error(
      `Dropdown.Portal compound components cannot be rendered outside the Dropdown.Portal component`
    );
  }
  return context;
}

export function Portal(props) {
  const { children, timeout } = props;
  const { isOpen, refTrigger, refMenu } = useDropdownContext();

  const [show, setShow] = React.useState(isOpen);
  const [render, setRender] = React.useState(isOpen);
  const [top, setTop] = React.useState(0);
  const [left, setLeft] = React.useState(0);

  const timeoutRef = React.useRef();
  const renderRef = React.useRef(render);

  useOnClickOutside(refMenu, () => {
    if (renderRef.current === isOpen) {
      requestAnimationFrame(() => {
        setShow(false);
      });
    }

    console.log(refMenu);
    // requestAnimationFrame(() => {
    //   setShow(false);
    // });
  });

  React.useEffect(() => {
    const offset = refTrigger.current.getBoundingClientRect();
    setTop(offset.top);
    setLeft(offset.left);
  }, [refTrigger]);

  React.useEffect(() => {
    if (renderRef.current !== isOpen) {
      requestAnimationFrame(() => {
        setShow(isOpen);
      });
    }

    if (isOpen) {
      setRender(true);
    }

    renderRef.current = isOpen;

    return () => {
      if (!renderRef.current) {
        setShow(isOpen);
      }
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
      timeoutRef.current = setTimeout(() => {
        if (!renderRef.current) {
          setRender(false);
        }
        clearTimeout(timeoutRef.current);
      }, timeout);
    };
  }, [isOpen, timeout]);

  const contextValue = React.useMemo(
    () => ({
      show,
    }),
    [show]
  );

  if (render) {
    return ReactDOM.createPortal(
      <PortalContext.Provider value={contextValue}>
        <PortalUI top={top} left={left}>
          {children}
        </PortalUI>
      </PortalContext.Provider>,
      document.querySelector("#portal")
    );
  } else return null;

  // return (
  // <PortalContext.Provider value={contextValue}>
  //   {render && <PortalUI>{children}</PortalUI>}
  // </PortalContext.Provider>
  // );
}

Portal.defaultProps = {
  timeout: 150,
};

export default Portal;
