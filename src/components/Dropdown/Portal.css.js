import styled from "styled-components";


export const PortalUI = styled("div")`
  position: fixed;
  bottom: 10px;
  right: 10px;

  @media (min-width: 980px) {
    top: ${(props) => props.top}px;
    left: ${(props) => props.left}px;
    bottom: unset;
    right: unset;
  }
`;
