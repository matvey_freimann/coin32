import React from "react";
import GameItem from "./GameItem";

const GameList = ({ games }) => {
  return (
    <div>
      {games.map(({ id, name }) => (
        <GameItem key={id} name={name} />
      ))}
    </div>
  );
};

export default GameList;
