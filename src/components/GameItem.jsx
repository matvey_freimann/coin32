import React from "react";
import PropTypes from "prop-types";

const GameItem = ({ name }) => {
  return <div>{name}</div>;
};

GameItem.propTypes = {
  name: PropTypes.string.isRequired,
};

export default GameItem;
