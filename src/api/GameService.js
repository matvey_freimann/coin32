import HttpService from "./HttpService";

export default class GameService {
  static async getList(search) {
    const response = await HttpService.get("/games", {
      params: {
        search,
      },
    });
    return response.data;
  }
}
