import HttpService from "./HttpService";

export default class GameService {
  static async getList() {
    const response = await HttpService.get("/platforms/lists/parents");
    return response;
  }
}
